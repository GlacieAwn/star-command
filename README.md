# Dependencies
- gnu make<br>
- devkitpro<br>
- libtonc<br>

# build instructions
make sure you have devkitpro and make installed, and type `make`. it should produce a binary that you can run in an emulator(I recommend mgba) or on real hardware using a flash cart.

# cool people
[sylvie](https://zlago.github.io/) - my sweet girlfriend. she helps with a lot of things, but for this one, helped me figure out OpenMPT for music :3

[AntonioND](https://github.com/AntonioND) - gbadev member. helped me with an issue I had with maxmod, and generally *really* helpful

the entire gbadev community - this wouldn't be possible without all of you! thanks :3