#include <tonc.h>
#include <maxmod.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "actors/player.h"
#include "include/defines.h"
#include "include/utils.h"
#include "soundbank.h"
#include "soundbank_bin.h"

int curState = GAMEPLAY;
struct Ship player;

void init();
void update();

int main(){

	init();

	while (1) {
		
	}

}

void init(){
	irq_init(NULL);
	irq_set(II_VBLANK, vblankHandler, 0);
	irq_enable(II_VBLANK);
	
	mmInitDefault((mm_addr)soundbank_bin, 32);
	//mmStart(0, MM_PLAY_LOOP);


	// init objects
	initPlayer(&player);
	REG_DISPCNT = DCNT_MODE0 | DCNT_BG0 | DCNT_OBJ | DCNT_OBJ_1D;
}

void update(){
	VBlankIntrWait();
	mmFrame();

	switch(curState){
		case SPLASH:{
			
		} break;
		case TITLE:{

		} break;
		case GAMEPLAY:{

		} break;
		case GAMEOVER:{
			updatePlayer(&player);
		} break;
		default:{

		}
	}
}