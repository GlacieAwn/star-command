#include <stdio.h>
#include <stdlib.h>

#define MAX_BULLETS          3
#define MAX_COMETS           3

enum gameState {
	SPLASH,
	TITLE,
	GAMEPLAY,
	GAMEOVER
};