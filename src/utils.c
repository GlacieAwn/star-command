#include "include/utils.h"
#include <stdio.h>
#include <stdlib.h>
#include <maxmod.h>

int randRange(int min, int max){
	return (rand() % (max - min + 1)) + min;
}

void vblankHandler(){
	mmVBlank();

	// anything that *has* to run inside of vblank like oamdma copy should go here.
}