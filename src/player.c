#include "actors/player.h"
#include <tonc.h>

void initPlayer(struct Ship *this){
	this->x           = 0;
	this->y           = 0;
	this->tID         = 0;
	this->pb          = 0;
	this->lives       = 3;
	this->bulletCount = 3;
	this->speed       = 0.8;
}

void updatePlayer(struct Ship *this){
	
}