#include <tonc.h>
#include <stdio.h>
#include <stdint.h>          // needed for uint8_t

struct Bullet{
	uint8_t x, y;
	uint8_t tID, pb;
	uint8_t isShot;          // 1 if shot, 0 if not
	double speed;

};