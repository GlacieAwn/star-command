#include <tonc.h>
#include <stdio.h>
#include <stdint.h>          // needed for uint8_t

struct Ship{
	uint8_t x, y;
	uint8_t tID, pb;
	uint8_t lives, bulletCount;
	double speed;
};

void initPlayer(struct Ship *this);
void updatePlayer(struct Ship *this);