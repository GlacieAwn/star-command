#include <tonc.h>
#include <stdio.h>
#include <stdint.h>          // needed for uint8_t

struct Comet{
	uint8_t x, y;
	uint8_t tID, pb;
	uint8_t cometCount;
	double speed;
};